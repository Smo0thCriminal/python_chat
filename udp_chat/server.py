import socket
import select

IP = '127.0.0.1'
PORT = 5000
BUFFER_SIZE = 8192


def server_start():
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind((IP, PORT))
    return sock


def send(serv_sock, connect, ((message, send_socket))):
    if connect is not send_socket:
        print('{0}::{1}'.format(send_socket, message))
        serv_sock.sendto(message, connect)


message_queue = []

if __name__ == '__main__':
    server_sock = server_start()
    clients_list = []
    server_list = [server_sock]
    message_queue = []

    while 1:
        read_sock, write_sock, exc_sock = select.select(server_list, [], [])

        for r in read_sock:
            msg, address = server_sock.recvfrom(BUFFER_SIZE)
            message_queue.append((msg, address))
            if address not in clients_list:
                clients_list.append(address)

            if message_queue:
                for x in message_queue:
                    for c in clients_list:
                        send(server_sock, c, x)

            message_queue = []
