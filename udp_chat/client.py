import socket
import select
import sys

UDP_IP = "127.0.0.1"
UDP_PORT = 5000
BUFFER_SIZE = 8192
address = (UDP_IP, UDP_PORT)


def client_start():
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    print('CLIENT: started on {0}:{1}'.format(UDP_IP, UDP_PORT))
    return sock


def prompt():
    sys.stdout.write('<You> ')
    sys.stdout.flush()


if __name__ == '__main__':
    client_sock = client_start()
    client_sock.sendto('INIT MESSAGE', (UDP_IP, UDP_PORT))
    socket_list = [client_sock, sys.stdin]
    while 1:
        r_sock, w_sock, e_sock = select.select(socket_list, [], [])

        for r in r_sock:
            if r is client_sock:
                msg, addr = client_sock.recvfrom(BUFFER_SIZE)
                print('{0}::{1}'.format(addr, msg))

            else:
                msg = sys.stdin.readline()
                client_sock.sendto(msg, (UDP_IP, UDP_PORT))
                prompt()
