import socket
import select

IP = '127.0.0.1'
PORT = 5000
BUFFER_SIZE = 4096


def server_start():
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.bind((IP, PORT))
    sock.listen(5)
    print('Server started at: {0}:{1}'.format(IP, PORT))
    return sock


def send(((message, send_socket), connect)):
    if connect is not send_socket:
        connect.send('{0}:{1}'.format(send_socket, message))


if __name__ == '__main__':
    server_sock = server_start()

    socket_list = [server_sock]
    message_queue = []

    while 1:
        readable, writable, exceptionable = select.select(socket_list, socket_list, socket_list)

        for r in readable:
            if r is server_sock:
                connection, client_address = server_sock.accept()
                connection.setblocking(0)
                print('SERVER: Got incoming transmission from: {}'.format(client_address))
                socket_list.append(connection)
            else:
                data = r.recv(BUFFER_SIZE)
                if data:
                    message_queue.append((data, r))

        for w in writable:
            if message_queue:
                for msg in message_queue:
                    send((msg, w))

        message_queue = []
