import socket
import sys
import select


HOST = '127.0.0.1'
PORT = 5000
BUFFER_SIZE = 4096
NAME = ''


def prompt():
    sys.stdout.write('<You> ')
    sys.stdout.flush()


def client_start():
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((HOST, PORT))
        print('Client started at: {0}:{1}'.format(HOST, PORT))
        return sock


if __name__ == '__main__':
    client_sock = client_start()
    name = raw_input('name: ')

    socket_list = [sys.stdin, client_sock]

    working = True

    while working:
        read_sockets, write_sockets, error_sockets = select.select(socket_list, [], [])

        for s in read_sockets:
            # incoming message from remote server
            if s == client_sock:
                data = s.recv(BUFFER_SIZE)
                if not data:
                    print '\nDisconnected from chat server'
                    working = False
                    break
                else:
                    # print data
                    sys.stdout.write(data)
                    prompt()

            # user entered a message
            else:
                msg = sys.stdin.readline()
                client_sock.send(msg)
                prompt()
